(defpackage extsort
  (:use :common-lisp)
  (:export :extsort))

(in-package :extsort)

;; utility

(defun close-all-streams (streams)
  (if (not (null streams))
    (progn (unwind-protect (progn (close (car streams)) (close-all-streams (cdr streams)))
                 (close-all-streams (cdr streams))))))

(defmacro with-open-files ((streams files &rest keys) &rest body)
  `(let ((,streams nil))
    (unwind-protect
      (progn
        (setq ,streams (mapcar (lambda (fl) (open fl ,@keys)) ,files))
        ,@body)
    (close-all-streams ,streams))))

;; external sort phase 1 code

(defstruct buffer-phase1
  "Internal data structure used in the first phase of external sorting while reading the origin file in chunks.
The size field is the number of the elements in the second field lines, the fields line is a list of strings."
  size lines)

(defun consume-to-reversed-buffer (stream max-line-proc)
  "Reads and consumes max-line-proc number of lines from the stream and returns the appropriate buffer-phase1 element whose
lines are in the reversed order than those coming from the stream. It is safe to call even if less than max-line-proc number
of lines are left on the stream."
  (let ((read-lines nil)
        (num-lines 0))
    (loop
      (let ((line (read-line stream nil)))
        (if (null line) (return))
        (setq read-lines (cons line read-lines))
        (setq num-lines (1+ num-lines))
        (if (= num-lines max-line-proc) (return))))
    (make-buffer-phase1 :size num-lines :lines read-lines)))

(defun flush-buffer-to-file (buffer outputdir)
  "Writes a buffer-phase1 buffer into a newly generated file in the outputdir directory. Returns the pathname of the generated file."
  (let ((output-pathname (gen-next-pathname outputdir)))
    (with-open-file (stream output-pathname :direction :output :if-does-not-exist :create)
      (mapcar (lambda (x) (write-line x stream)) (buffer-phase1-lines buffer)))
    output-pathname))

(defun gen-next-pathname (dir)
  (gen-next-pathname-rec dir 0))

(defun gen-next-pathname-rec (dir i)
  (let ((res (make-pathname :name (concatenate 'string "extsort-tmp-" (write-to-string i))
                            :type nil
                            :defaults dir)))
    (if (not (probe-file res)) res
        (gen-next-pathname-rec dir (1+ i)))))


(defun write-sorted-batches (origin-file outputdir comparator max-line-proc)
  "Reads the origin-file until max-line-proc number of lines is reached, performs an in-memory sort by comparator then writes
the sorted output into a generated batch file in the outputdir directory; repeats all the steps until the read from origin-file
is completed. Returns the list of written sorted files."
  (with-open-file (stream origin-file)
    (write-sorted stream outputdir comparator max-line-proc nil)))

(defun write-sorted (stream outputdir comparator max-line-proc accumulator)
  (let* ((buffer (consume-to-reversed-buffer stream max-line-proc))
         (sorted-lines (sort (buffer-phase1-lines buffer) comparator)))
    (setf (buffer-phase1-lines buffer) sorted-lines)
    (if (= 0 (buffer-phase1-size buffer))
        accumulator
        (let* ((written-file (flush-buffer-to-file buffer outputdir))
               (new-accumulator (cons written-file accumulator)))
          (write-sorted stream outputdir comparator max-line-proc new-accumulator)))))

;; external sort phase 2 code

(defstruct buffer-phase2
  "Internal data structure used to perform the final k-way merge sort. The flag complete becomes T when no more lines are on
the stream, the current-size is the length of the lines in the buffer and the max-size is a fixed maximum number of lines that
the buffer can accomodate."
  stream lines completed current-size max-size)

(defun load-buffer2 (buffer)
  "Performs the reads on the buffer stream to the maximum size of the buffer, filling the lines and updating the other slots accordingly."
  (let ((line (read-line (buffer-phase2-stream buffer) nil)))
    (cond ((null line) (setf (buffer-phase2-completed buffer) t)
                       (setf (buffer-phase2-lines buffer) (reverse (buffer-phase2-lines buffer))))
          (t (setf (buffer-phase2-lines buffer) (cons line (buffer-phase2-lines buffer)))
             (setf (buffer-phase2-current-size buffer) (1+ (buffer-phase2-current-size buffer)))
             (if (= (buffer-phase2-max-size buffer) (buffer-phase2-current-size buffer))
                 (setf (buffer-phase2-lines buffer) (reverse (buffer-phase2-lines buffer)))
                 (load-buffer2 buffer))))))

(defun peek (buffer)
  "Reads the top element of the buffer, without remove. If the buffer is empty but not completed it will load the buffer.
If the buffer is empty and completed returns NIL."
  (if (> (buffer-phase2-current-size buffer) 0)
      (car (buffer-phase2-lines buffer))
      (cond ((buffer-phase2-completed buffer) nil)
            (t (load-buffer2 buffer) (peek buffer)))))

(defun poll (buffer)
  "Reads and removes the top element of the buffer."
  (let ((top (car (buffer-phase2-lines buffer)))
        (remain (cdr (buffer-phase2-lines buffer))))
       (setf (buffer-phase2-lines buffer) remain)
       (setf (buffer-phase2-current-size buffer) (1- (buffer-phase2-current-size buffer)))
       top))

(defun k-way-merge (comparator buffers target-stream)
  "Reads at all the top elements from a list of buffer-phase2 buffers, then pull the first according to comparator and writes it
into the target-stream. Repeats the process until all the buffers are exhausted (completed slot)."
  (if (not (every (lambda (b) (null (peek b))) buffers))
      (let* ((non-exhausted-buffers (remove-if (lambda (b) (null (peek b))) buffers))
             (reducer (lambda (b1 b2) (if (funcall comparator (peek b1) (peek b2)) b1 b2)))
             (line (poll (reduce reducer non-exhausted-buffers))))
        (write-line line target-stream)
        (k-way-merge comparator buffers target-stream))))

;; main

(defgeneric extsort (origin-file target-file comparator max-line-proc)
  (:documentation "Sorts the lines of origin-file, by comparator function, that cannot fit entirely in memory. The sorted ouput file is
written into target-file. The max-line-proc integer expresses the max number of lines to be processed in memory by extsort.
Returns the target-file."))

(defmethod extsort ((origin-file string) (target-file string) comparator max-line-proc)
  (extsort (parse-namestring origin-file) (parse-namestring target-file) comparator max-line-proc))

(defmethod extsort ((origin-file pathname) (target-file pathname) comparator max-line-proc)
  (let* ((output-dir (make-pathname :name nil :type nil :defaults target-file))
         (sorted-batches (write-sorted-batches origin-file output-dir comparator max-line-proc))
         (phase2-buffer-size (max 1 (multiple-value-bind (x y) (floor (/ max-line-proc (length sorted-batches)))
                                                               (declare (ignorable y)) x))))
    (with-open-file (target-stream target-file :direction :output :if-does-not-exist :create)
      (with-open-files (streams sorted-batches)
        (let ((phase2-buffers (mapcar (lambda (s) (make-buffer-phase2 :stream s :lines nil :completed nil :current-size 0 :max-size phase2-buffer-size))
                                      streams)))
          (k-way-merge comparator phase2-buffers target-stream))))
    (mapcar #'delete-file sorted-batches)
    'target-file))
