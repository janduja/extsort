<html>
<head>
<link rel="stylesheet" type="text/css" href="default.css">
<meta charset="utf-8">
<meta name="description" content="Author: Raffaele Arecchi, Software: An external sorting library for Common Lisp">
<title>extsort 1.0: external sorting library for Common Lisp</title>
<style type="text/css">pre { padding:10px; background-color:#e0ebeb;)</style>
</head>

<body lang="en">

<h1>extsort 1.0: external sorting library for Common Lisp</h1>

<p><b>extsort</b> is a Common Lisp library for external sorting of Unicode text files. External sorting is a sorting
process where the data to be sorted cannot fit entirely in memory: this is the case, for example, of big files.</p>

<p>Source code is released under a <a href="https://opensource.org/licenses/MIT">MIT License</a>
and hosted at git repository <code>https://gitlab.com/janduja/extsort.git</code> under branch <code>1.0</code>.</p>
<p>A compressed zip package can be downloaded at <a href="http://janduja.com/extsort/extsort-1.0-tar.gz">
http://janduja.com/extsort/extsort-1.0-tar.gz</a>.</p>

<p>Contents:
<ol>
<li><a href="#install">Installation</a></li>
<li><a href="#use">Use</a></li>
<li><a href="#internals">Internals</a></li>
<li><a href="#testcomp">Tests and compatibility</a></li>
</ol>
</p>

<h3><a id="install">Installation</a></h3>
<p>The library has no dependencies and can be loaded directly or as an <a href="http://www.cliki.net/asdf">ASDF</a> package.
First, get the source code or unpack the zip file.</p>
<p>To load directly:
<pre>(load "extsort.lisp")</pre>
To load as an ASDF system, ensure that the library source is located in a directory looked by ASDF (ie. <code>~/common-lisp/extsort</code>),
then in your code:
<pre>(asdf:load-system :extsort)</pre>
</p>

<h3><a id="use">Use</a></h3>
<p>[Generic] <code><b>extsort</b> origin-file target-file comparator max-line-proc => target-file</code><br/>
Sorts the lines of origin-file, by comparator function, that cannot fit entirely in memory. The sorted ouput file is
written into target-file. The <i>integer</i> max-line-proc expresses the max number of lines to be processed in memory by extsort.
Returns the target-file.</p>
<p>[Method] <code><b>extsort</b> (<i>string</i>,<i>string</i>)</code><br/>
Converts the string to pathname by <code>parse-namestring</code> then recall <code>extsort</code></p>

<p>[Method] <code><b>extsort</b> (<i>pathname</i>,<i>pathname</i>)</code></p>

<p>The following example supposes the existence of a file containing a list of countries with their GDP in this format:
<pre>Taiwan|1177052
Austria|432424
South Africa|761926
Philippines|878980
...</pre>
This call performs an alphabetical sort of the file, processing a maximum number of 100 lines in memory, 
and writes the result into a file <i>alph_result</i> in relative folder <i>output</i>:
<pre>(extsort "countries_gdp_2017" "output/alph_result" #'string&lt; 100)</pre>
This call use the regex library <a href="http://www.cliki.net/CL-PPCRE">cl-ppcre</a> and performs an ascending GDP sorting,
processing a maximum number of 50 lines in memory:
<pre>(let* ((get-gdp (lambda (x) (parse-integer (cadr (cl-ppcre:split "\\|" x)))))
       (comparator (lambda (x y) (&lt; (funcall get-gdp x) (funcall get-gdp y)))))
  (extsort "countries_gdp_2017" "output/asc_gdp_result" comparator 50))</pre>
</p>


<h3><a id="internals">Internals</a></h3>
<p>The external sort is performed in two phases: first the input file is scanned sequentially to produce temporary smaller 
sorted files, then these files are merged togheter via a k-way merge to produce the final result.</p>
<h4>Phase 1</h4>
<img src="extsort-phase1.svg" width="600"/>
<p>The steps in the first phase are:
<ol>
<li>the input file is read into a buffer (structure <code>buffer-phase1</code>) containing
a maximum number of lines as specified by parameter <code>max-line-proc</code> in function <code>extsort</code></li>
<li>the buffer lines get sorted according to the <code>comparator</code> parameter in <code>extsort</code></li>
<li>the sorted lines are written into a new file located in the same directory as the future final file</li>
<li>all the previous steps are repeated until the input file read is completed</li>
</ol>
</p>
<h4>Phase 2</h4>
<p>In the second phase a list of buffers (structure <code>buffer-phase2</code>) is built for each temporary file,
the capacity of each being the <code>max-line-proc</code> parameter of <code>extsort</code> divided by the number of temporary files
(floor rounded).</p>
<p>A k-way merge is performed peeking (function <code>peek</code>) the top element of each buffer and choosing the first
according to the comparator parameter in <code>extsort</code>. The chosen element is then removed from its buffer
(function <code>poll</code>) and written to the final file. The buffers automatically get loaded, when empty,
to the maximum of their capacity.
</p>
<p>The process terminates when no more lines che be read by the buffers.</p>
<img src="extsort-phase2.svg" width="1000"/>

<h3><a id="testcomp">Tests and compatibility</a></h3>
<p>Tests are collected in file <code>extsort-test.lisp</code> under the test directory and have dependencies on
<a href="http://www.cliki.net/lisp-unit">lisp-unit</a> and <a href="http://www.cliki.net/CL-PPCRE">cl-ppcre</a>.</p>
<p>To run the tests you have to position as current directory in the <code>extsort/test</code>, then launch
your Common Lisp implementation and load the file <code>extsort-test.lisp</code>. A Unit Test report is printed on the
standard output telling the run tests, the passed and failed assertions and the execution errors.</p>
<p>The tests run successfully with the following Common Lisp implementations and hardware configuration:
<ul>
<li>SBCL 1.2.4.debian</li>
<li>GNU CLISP 2.49 (2010-07-07)</li>
</ul>
<code>
<table border="1">
<tr>
<td>[uname]</td><td>Debian 3.16.36-1+deb8u2 (2016-10-19) i686 GNU/Linux</td>
</tr>
<tr>
<td>[/proc/cpuinfo]</td><td>model name : Intel(R) Pentium(R) M processor 1.70GHz</td>
</tr>
<tr>
<td>[/proc/cpuinfo]</td><td>cache size : 2048 KB</td>
</tr>
<tr>
<td>[/proc/cpuinfo]</td><td>address sizes : 32 bits physical, 32 bits virtual</td>
</tr>
<tr>
<td>[cpufreq-info]</td><td>hardware limits: 600 MHz - 1.60 GHz</td>
</tr>
<tr>
<td>[/proc/meminfo]</td><td>MemTotal: 511492 kB</td>
</tr>
</table>
</code>
</p>

<hr>
Last edited on: 13 May 2017<br/>
Author: Raffaele Arecchi<br/>
Contact the author of this document via email to: <code>raffaele.arecchi {at} gmail {point} com</code>.
