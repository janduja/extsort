(in-package :asdf)

(defsystem :extsort
  :description "External sorting"
  :version "1.0"
  :author "Raffaele Arecchi <raffaele.arecchi@gmail.com>"
  :license "MIT"
  :components
  ((:file "extsort")))
