;#!/usr/bin/sbcl --script
;(declaim (optimize (debug 3)))
;(require 'asdf)

(asdf:load-system :lisp-unit)
(asdf:load-system :cl-ppcre)
(asdf:load-system :extsort)

(in-package :extsort)

(let* ((sym-to-export '("consume-to-reversed-buffer"
                        "flush-buffer-to-file"
                        "gen-next-pathname"
                        "write-sorted-batches"
                        "buffer-phase1-size"
                        "buffer-phase1-lines"
                        "make-buffer-phase1"
                        "make-buffer-phase2"
                        "load-buffer2"
                        "buffer-phase2-lines"
                        "buffer-phase2-completed"
                        "buffer-phase2-current-size"
                        "peek"
                        "poll"
                        "k-way-merge"
                        "with-open-files"))
       (pack (find-package :extsort))
       (is-extsort-sym (lambda (x) (and (eql (symbol-package x) pack)
                                        (member (symbol-name x) sym-to-export :test #'string-equal)))))
  (do-all-symbols (sym pack) (when (funcall is-extsort-sym sym) (export sym))))

(defpackage extsort-test
  (:use :common-lisp :lisp-unit :extsort))

(in-package :extsort-test)

(defparameter *countries-gdp-file-path*
  (make-pathname :directory (pathname-directory *load-pathname*)
                 :name "countries_gdp_2017"
                 :type nil))

(defparameter *outputdir-file-path*
  (merge-pathnames
    (make-pathname :directory '(:relative "output"))
    (make-pathname :directory (pathname-directory *load-pathname*))))

(defun clean-and-prepare-for-test ()
  (let ((dir-content (directory (make-pathname :directory (pathname-directory *outputdir-file-path*)
                                               :name :wild))))
    (if dir-content (mapcar #'delete-file dir-content)
        (ensure-directories-exist *outputdir-file-path*))))

(define-test consume-to-reversed-buffer-initial-test
  (let ((actual nil))
    (with-open-file (stream *countries-gdp-file-path*)
      (setq actual (consume-to-reversed-buffer stream 3)))
    (assert-equal 3 (buffer-phase1-size actual))
    (assert-equal 3 (length (buffer-phase1-lines actual)))
    (assert-equal "India|9489302" (nth 0 (buffer-phase1-lines actual)))
    (assert-equal "United States|19417144" (nth 1 (buffer-phase1-lines actual)))
    (assert-equal "China|23194411" (nth 2 (buffer-phase1-lines actual)))))

(define-test consume-to-reversed-buffer-second-group-test
  (let ((actual nil))
    (with-open-file (stream *countries-gdp-file-path*)
      (consume-to-reversed-buffer stream 3)
      (setq actual (consume-to-reversed-buffer stream 3)))
    (assert-equal 3 (buffer-phase1-size actual))
    (assert-equal 3 (length (buffer-phase1-lines actual)))
    (assert-equal "Russia|3938001" (nth 0 (buffer-phase1-lines actual)))
    (assert-equal "Germany|4134668" (nth 1 (buffer-phase1-lines actual)))
    (assert-equal "Japan|5420228" (nth 2 (buffer-phase1-lines actual)))))

(define-test consume-to-reversed-buffer-all-file-test
  (let ((actual-1 nil)
        (actual-2 nil)
        (actual-3 nil))
    (with-open-file (stream *countries-gdp-file-path*)
      (setq actual-1 (consume-to-reversed-buffer stream 100))
      (setq actual-2 (consume-to-reversed-buffer stream 100))
      (setq actual-3 (consume-to-reversed-buffer stream 100)))
    (assert-equal 100 (buffer-phase1-size actual-1))
    (assert-equal "Democratic Republic of the Congo|68331" (nth 0 (buffer-phase1-lines actual-1)))
    (assert-equal "China|23194411" (nth 99 (buffer-phase1-lines actual-1)))
    (assert-equal 91 (buffer-phase1-size actual-2))
    (assert-equal "Syria|-1" (nth 0 (buffer-phase1-lines actual-2)))
    (assert-equal "Paraguay|68005" (nth 90 (buffer-phase1-lines actual-2)))
    (assert-equal 0 (buffer-phase1-size actual-3))
    (assert-true (null (buffer-phase1-lines actual-3)))))

(define-test gen-next-pathname-clean-dir-test
  (clean-and-prepare-for-test)
  (assert-equal "extsort-tmp-0" (file-namestring (gen-next-pathname *outputdir-file-path*))))

(define-test gen-next-pathname-existing-files-test
  (clean-and-prepare-for-test)
  (dolist (idx '("0" "1"))
    (with-open-file (stream (make-pathname :directory (pathname-directory *outputdir-file-path*)
                                           :name (concatenate 'string "extsort-tmp-" idx)
                                           :type nil)
                     :direction :output :if-does-not-exist :create)
        (write-line "test" stream)))
  (assert-equal "extsort-tmp-2" (file-namestring (gen-next-pathname *outputdir-file-path*))))

(define-test flush-buffer-to-file-test
  (clean-and-prepare-for-test)
  (let* ((lines '("first line" "second line" "third line"))
         (buffer (make-buffer-phase1 :size 3 :lines lines))
         (written-file (flush-buffer-to-file buffer *outputdir-file-path*)))
    (assert-equal "extsort-tmp-0" (file-namestring written-file))
    (with-open-file (stream written-file)
      (assert-equal "first line" (read-line stream))
      (assert-equal "second line" (read-line stream))
      (assert-equal "third line" (read-line stream))
      (assert-equal :eof (read-line stream nil :eof)))))

(defstruct file-start-end-info num-of-lines first-line last-line)

(defun file-info (filepath)
  (let ((result (make-file-start-end-info :num-of-lines 0 :first-line "" :last-line "")))
    (with-open-file (stream filepath)
      (do ((line (read-line stream nil) (read-line stream nil)))
        ((null line) result)
        (if (= 0 (file-start-end-info-num-of-lines result)) (setf (file-start-end-info-first-line result) line))
        (setf (file-start-end-info-last-line result) line)
        (setf (file-start-end-info-num-of-lines result) (1+ (file-start-end-info-num-of-lines result)))))))

(define-test write-alphabetical-sorted-big-batches
  (clean-and-prepare-for-test)
  (let ((batches (write-sorted-batches *countries-gdp-file-path* *outputdir-file-path* #'string< 100)))
    (assert-equal 2 (length batches))
    (assert-equal "extsort-tmp-1" (file-namestring (nth 0 batches)))
    (assert-equal "extsort-tmp-0" (file-namestring (nth 1 batches)))
    (assert-equal 91 (file-start-end-info-num-of-lines (file-info (nth 0 batches))))
    (assert-equal "Afghanistan|67462" (file-start-end-info-first-line (file-info (nth 0 batches))))
    (assert-equal "Zimbabwe|29795" (file-start-end-info-last-line (file-info (nth 0 batches))))
    (assert-equal 100 (file-start-end-info-num-of-lines (file-info (nth 1 batches))))
    (assert-equal "Algeria|634746" (file-start-end-info-first-line (file-info (nth 1 batches))))
    (assert-equal "Zambia|68648" (file-start-end-info-last-line (file-info (nth 1 batches))))))

(define-test write-reverse-gdp-sorted-big-batches
  (clean-and-prepare-for-test)
  (let* ((get-gdp (lambda (x) (parse-integer (cadr (cl-ppcre:split "\\|" x)))))
         (comparator (lambda (x y) (< (funcall get-gdp x) (funcall get-gdp y))))
         (batches (write-sorted-batches *countries-gdp-file-path* *outputdir-file-path* comparator 100)))
    (assert-equal 2 (length batches))
    (assert-equal "extsort-tmp-1" (file-namestring (nth 0 batches)))
    (assert-equal "extsort-tmp-0" (file-namestring (nth 1 batches)))
    (assert-equal 91 (file-start-end-info-num-of-lines (file-info (nth 0 batches))))
    (assert-equal "Syria|-1" (file-start-end-info-first-line (file-info (nth 0 batches))))
    (assert-equal "Paraguay|68005" (file-start-end-info-last-line (file-info (nth 0 batches))))
    (assert-equal 100 (file-start-end-info-num-of-lines (file-info (nth 1 batches))))
    (assert-equal "Democratic Republic of the Congo|68331" (file-start-end-info-first-line (file-info (nth 1 batches))))
    (assert-equal "China|23194411" (file-start-end-info-last-line (file-info (nth 1 batches))))))

(define-test write-alphabetical-sorted-small-batches
  (clean-and-prepare-for-test)
  (let ((batches (write-sorted-batches *countries-gdp-file-path* *outputdir-file-path* #'string< 50)))
    (assert-equal 4 (length batches))
    (assert-equal "extsort-tmp-3" (file-namestring (nth 0 batches)))
    (assert-equal "extsort-tmp-2" (file-namestring (nth 1 batches)))
    (assert-equal "extsort-tmp-1" (file-namestring (nth 2 batches)))
    (assert-equal "extsort-tmp-0" (file-namestring (nth 3 batches)))
    (assert-equal 41 (file-start-end-info-num-of-lines (file-info (nth 0 batches))))
    (assert-equal 50 (file-start-end-info-num-of-lines (file-info (nth 1 batches))))
    (assert-equal 50 (file-start-end-info-num-of-lines (file-info (nth 2 batches))))
    (assert-equal 50 (file-start-end-info-num-of-lines (file-info (nth 3 batches))))
    (assert-equal "Algeria|634746" (file-start-end-info-first-line (file-info (nth 3 batches))))
    (assert-equal "Vietnam|648243" (file-start-end-info-last-line (file-info (nth 3 batches))))
    (assert-equal "Angola|193935" (file-start-end-info-first-line (file-info (nth 2 batches))))
    (assert-equal "Zambia|68648" (file-start-end-info-last-line (file-info (nth 2 batches))))
    (assert-equal "Afghanistan|67462" (file-start-end-info-first-line (file-info (nth 1 batches))))
    (assert-equal "Zimbabwe|29795" (file-start-end-info-last-line (file-info (nth 1 batches))))
    (assert-equal "Antigua and Barbuda|2372" (file-start-end-info-first-line (file-info (nth 0 batches))))
    (assert-equal "Vanuatu|772" (file-start-end-info-last-line (file-info (nth 0 batches))))))

(define-test load-buffer2-test
  (with-open-file (stream *countries-gdp-file-path*)
    (let ((buffer (make-buffer-phase2 :stream stream :lines nil :completed nil :current-size 0 :max-size 4)))
       (load-buffer2 buffer)
       (assert-equal "China|23194411" (nth 0 (buffer-phase2-lines buffer)))
       (assert-equal "Japan|5420228" (nth 3 (buffer-phase2-lines buffer)))
       (assert-false (buffer-phase2-completed buffer))
       (assert-equal 4 (buffer-phase2-current-size buffer)))))

(define-test peek-already-loaded-buffer2-test
  (with-open-file (stream *countries-gdp-file-path*)
    (let ((buffer (make-buffer-phase2 :stream stream :lines nil :completed nil :current-size 0 :max-size 4)))
       (load-buffer2 buffer)
       (assert-equal "China|23194411"  (peek buffer))
       (assert-equal "China|23194411" (nth 0 (buffer-phase2-lines buffer)))
       (assert-equal "Japan|5420228" (nth 3 (buffer-phase2-lines buffer)))
       (assert-equal 4 (buffer-phase2-current-size buffer)))))

(define-test poll-already-loaded-buffer2-test
  (with-open-file (stream *countries-gdp-file-path*)
    (let ((buffer (make-buffer-phase2 :stream stream :lines nil :completed nil :current-size 0 :max-size 4)))
       (load-buffer2 buffer)
       (assert-equal "China|23194411"  (poll buffer))
       (assert-equal "United States|19417144" (nth 0 (buffer-phase2-lines buffer)))
       (assert-equal "Japan|5420228" (nth 2 (buffer-phase2-lines buffer)))
       (assert-equal 3 (buffer-phase2-current-size buffer)))))

(define-test peek-should-reload-if-empty-buffer2-test
  (with-open-file (stream *countries-gdp-file-path*)
    (let ((buffer (make-buffer-phase2 :stream stream :lines nil :completed nil :current-size 0 :max-size 4)))
       (load-buffer2 buffer)
       (dotimes (i 4) (poll buffer))
       (assert-equal 0 (buffer-phase2-current-size buffer))
       (assert-equal nil (buffer-phase2-lines buffer))
       (assert-equal "Germany|4134668" (peek buffer))
       (assert-equal 4 (buffer-phase2-current-size buffer))
       (assert-equal "Brazil|3216031" (nth 3 (buffer-phase2-lines buffer))))))

(define-test load-should-set-completed-buffer2-test
  (with-open-file (stream *countries-gdp-file-path*)
    (let ((buffer (make-buffer-phase2 :stream stream :lines nil :completed nil :current-size 0 :max-size 100)))
       (load-buffer2 buffer)
       (assert-false (buffer-phase2-completed buffer))
       (assert-equal 100 (buffer-phase2-current-size buffer))
       (dotimes (i 100) (poll buffer))
       (load-buffer2 buffer)
       (assert-true (buffer-phase2-completed buffer))
       (assert-equal 91 (buffer-phase2-current-size buffer)))))


(define-test peek-return-nil-on-empty-completed-test
  (with-open-file (stream *countries-gdp-file-path*)
    (let ((buffer (make-buffer-phase2 :stream stream :lines nil :completed nil :current-size 0 :max-size 200)))
       (load-buffer2 buffer)
       (assert-true (buffer-phase2-completed buffer))
       (assert-equal 191 (buffer-phase2-current-size buffer))
       (dotimes (i 191) (poll buffer))
       (assert-equal 0 (buffer-phase2-current-size buffer))
       (assert-equal nil (peek buffer)))))

(defparameter *ftest1*
  (make-pathname :directory (pathname-directory *load-pathname*)
                 :name "k_way_merge_sort_1"
                 :type nil))
(defparameter *ftest2*
  (make-pathname :directory (pathname-directory *load-pathname*)
                 :name "k_way_merge_sort_2"
                 :type nil))

(define-test with-open-files-macro-test
  (let ((files (cons *ftest1* (cons *ftest2* nil))))
    (with-open-files (streams files)
      (assert-equal "China|23194411" (read-line (nth 0 streams)))
      (assert-equal "Canada|1752910" (read-line (nth 1 streams)))
      (assert-equal "India|9489302" (read-line (nth 0 streams)))
      (assert-equal "Iran|1535491" (read-line (nth 1 streams))))))

(define-test k-way-merge-test
  (clean-and-prepare-for-test)
  (let ((target-file (make-pathname :directory (pathname-directory *outputdir-file-path*)
                                    :name "k_way_merge_result" :type nil))
        (origin-files (cons *ftest1* (cons *ftest2* nil))))
    (with-open-file (target-stream target-file :direction :output :if-does-not-exist :create)
      (with-open-files (streams origin-files)
        (k-way-merge #'string<
                     (mapcar (lambda (s) (make-buffer-phase2 :stream s :lines nil :completed nil :current-size 0 :max-size 2)) streams)
                     target-stream)))
    (with-open-file (target-stream target-file)
      (assert-equal "Canada|1752910" (read-line target-stream))
      (assert-equal "China|23194411" (read-line target-stream))
      (assert-equal "India|9489302" (read-line target-stream))
      (assert-equal "Iran|1535491" (read-line target-stream)))
    (assert-equal 12 (file-start-end-info-num-of-lines (file-info target-file)))
    (assert-equal "United States|19417144" (file-start-end-info-last-line (file-info target-file)))))

(define-test extsort-alphabetical-sort-test
  (clean-and-prepare-for-test)
  (let ((target-file (make-pathname :directory (pathname-directory *outputdir-file-path*)
                                    :name "extsort_result" :type nil)))
    (extsort *countries-gdp-file-path* target-file #'string< 100)
    (with-open-file (target-stream target-file)
      (assert-equal "Afghanistan|67462" (read-line target-stream))
      (assert-equal "Albania|36198" (read-line target-stream))
      (assert-equal "Algeria|634746" (read-line target-stream))
      (assert-equal "Angola|193935" (read-line target-stream)))
      (assert-equal 191 (file-start-end-info-num-of-lines (file-info target-file)))
      (assert-equal "Zimbabwe|29795" (file-start-end-info-last-line (file-info target-file)))))

(define-test extsort-ascending-gdp-sort-test
  (clean-and-prepare-for-test)
  (let* ((target-file (make-pathname :directory (pathname-directory *outputdir-file-path*)
                                     :name "extsort_result" :type nil))
         (get-gdp (lambda (x) (parse-integer (cadr (cl-ppcre:split "\\|" x)))))
         (comparator (lambda (x y) (< (funcall get-gdp x) (funcall get-gdp y)))))
    (extsort *countries-gdp-file-path* target-file comparator 100)
    (with-open-file (target-stream target-file)
      (assert-equal "Syria|-1" (read-line target-stream))
      (assert-equal "Tuvalu|41" (read-line target-stream))
      (assert-equal "Marshall Islands|188" (read-line target-stream))
      (assert-equal "Kiribati|222" (read-line target-stream)))
      (assert-equal 191 (file-start-end-info-num-of-lines (file-info target-file)))
      (assert-equal "China|23194411" (file-start-end-info-last-line (file-info target-file)))))

(define-test extsort-alphabetical-sort-small-batches-test
  (clean-and-prepare-for-test)
  (let ((target-file (make-pathname :directory (pathname-directory *outputdir-file-path*)
                                    :name "extsort_result" :type nil)))
    (extsort *countries-gdp-file-path* target-file #'string< 10)
    (with-open-file (target-stream target-file)
      (assert-equal "Afghanistan|67462" (read-line target-stream))
      (assert-equal "Albania|36198" (read-line target-stream))
      (assert-equal "Algeria|634746" (read-line target-stream))
      (assert-equal "Angola|193935" (read-line target-stream)))
      (assert-equal 191 (file-start-end-info-num-of-lines (file-info target-file)))
      (assert-equal "Zimbabwe|29795" (file-start-end-info-last-line (file-info target-file)))))

(define-test extsort-alphabetical-sort-string-path-test
  (clean-and-prepare-for-test)
  (extsort "countries_gdp_2017" "output/extsort_result" #'string< 100)
  (let ((target-file (make-pathname :directory (pathname-directory *outputdir-file-path*)
                                    :name "extsort_result" :type nil)))
    (with-open-file (target-stream target-file)
      (assert-equal 191 (file-start-end-info-num-of-lines (file-info target-file)))
      (assert-equal "Afghanistan|67462" (file-start-end-info-first-line (file-info target-file)))
      (assert-equal "Zimbabwe|29795" (file-start-end-info-last-line (file-info target-file))))))


(setq *print-failures* t)
(setq *print-errors* t)
(run-tests :all)
